<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OperationRunnerBehavior
 *
 * @author vladislav
 */
namespace YiiOperation\behaviors;
use YiiComponents\behaviors\DependentBehavior;

class OperationRunnerBehavior extends DependentBehavior {
    
    protected $category = 'operation';
    
    protected $_operations = array();
    
    public $uniqueId = 'operations';
    public $abort = false;
    public $reset = false;
    public $force = false;
    public $rootMethod;
    public $operationClass = 'YiiOperation\components\Operation';

    
    protected function dependencies() {
        return array(
            'logger' => 'YiiComponents\behaviors\LoggerBehavior',
        );
    }
    
    public function run($method, $args = array(), $params = array()) 
    {
        $alias = $this->getAlias($method);
        $this->rootMethod = is_null($this->rootMethod) ? $method : $this->rootMethod;
        
        $this->logger->addTrace("operation alias: $alias", $this->category);
        
        $operation = $this->get($alias, $method, $args);

        if ($this->abort) {
            $this->logger->addTrace("aborting $alias...", $this->category);
            $operation->abort();
        }
        if ($this->reset) {
            $operation->reset($this->force);
            $this->logger->addTrace("resetting $alias...", $this->category);
        }

        try{
            
            $result = $operation->prepare()->execute();
            
        } catch (Exception $e) {
            
            if ($method === $this->rootMethod) {
                throw $e;
            } else {
                $this->logger->addError($e->getMessage(), $method);
                $result = false;
            }
            
        }
        
        return $result;
    }
    
    protected function getAlias($method) {
        return "$this->uniqueId.$method";
    }
    
    protected function get($alias, $method, $args = array()) {
        
        if (!isset($this->_operations[$alias])) {
            
            $this->logger->addTrace("initializing $method operation...", $this->category);
            $operationParams = $this->getOperationParams($alias, $method, $args);
            
            $this->_operations[$alias] = \Yii::createComponent($operationParams);
        } else {
            $this->_operations[$alias]->args = $args;
        }
        
        return $this->_operations[$alias];
    }
    
    public function getState($method = NULL) {
        $method = is_null($method) ? $this->rootMethod : $method;
        return $this->get($this->getAlias($method), $method)->stateObject;
    }
    
    protected function getOperationParams($alias, $method, $args) {
        
        return array(
            'class' => $this->getOperationClass($method),
            'alias' => $alias,
            'object' => $this->owner,
            'method' => 'operation'.ucfirst($method),
            'args' => $args,
            'loggerBehavior' => array(
                'class' => 'YiiComponents\behaviors\LoggerBehavior',
                'category' => "{$this->logger->category}.$method.operation",
                'live' => $this->logger->live,
            ),
        );
    }
    
    public function getOperationClass($method = NULL) {
        return $this->operationClass;
    }
}
