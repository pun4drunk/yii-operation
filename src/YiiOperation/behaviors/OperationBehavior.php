<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of LongPollingBehavior
 *
 * @author vladislav
 */
namespace YiiOperation\behaviors;
use CBehavior;

class OperationBehavior extends CBehavior {

    public $behaviors = array();
    public $resume = false;
    public $state;
    
    public function attach($owner) {
        
        if ($this->behaviors) {
            $this->attachBehaviors($this->behaviors);
        }
        
        parent::attach($owner);
    }
    
    
    public function abort() {
        if($this->hasEventHandler('onAbort'))
            $this->onAbort(new \CEvent($this));
        
        exit;
    }
    
    public function dead() {
        if ($this->hasEventHandler('onDead'))
            $this->onDead(new \CEvent($this));
            
    }

    public function onDead($event) {
        $this->raiseEvent('onDead', $event);
    }

    public function onAbort($event) {
        $this->raiseEvent('onAbort', $event);
    }
    
    public function beforeOperation() {
        
        if($this->hasEventHandler('onBeforeOperation'))
            $this->onBeforeOperation(new \CEvent($this));
        return true;
    }
    
    public function onBeforeOperation($event) {
        $this->raiseEvent('onBeforeOperation', $event);
    }
 
    public function afterOperation() {
        if($this->hasEventHandler('onAfterOperation'))
            $this->onAfterOperation(new \CEvent($this));
    }
    
    public function onAfterOperation($event) {
        $this->raiseEvent('onAfterOperation', $event);
    }
    
    public function beforeIteration() {
        
        $event = new \CEvent($this);
        $this->onBeforeIteration($event);
        
        return true;
    }
    
    public function afterIteration() {
        $event = new \CEvent($this);
        $this->onAfterIteration($event);
    }
    
    public function onBeforeIteration($event) {
        $this->raiseEvent('onBeforeIteration', $event);
    }
    
    public function onAfterIteration($event) {
        $this->raiseEvent('onAfterIteration', $event);
    }
}

