<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OperationStateAttributesBehavior
 *
 * @author vladislav
 */
namespace YiiOperation\behaviors;
use YiiOperation\helpers\OperationStatusHelper;

class OperationStateAttributesBehavior extends \CBehavior {
        
    public $statusAttribute = 'status';
    public $statusDefault;
    
    public function getDefaultStatus() {
        return is_null($this->statusDefault) ? OperationStatusHelper::STATUS_NOT_RUNNING: $this->statusDefault;
    }
    
    public function getIsScheduled() {
        return OperationStatusHelper::isScheduled($this->getStatus());
    }
    
    public function getIsQueued() {
        return OperationStatusHelper::isQueued($this->getStatus());
    }
    
    public function getIsRunning() {
        return OperationStatusHelper::isRunning($this->getStatus());
    }
    
    public function getIsFinished() {
        return OperationStatusHelper::isFinished($this->getStatus());
    }
    
    public function getIsAborted() {
        return OperationStatusHelper::isAborted($this->getStatus());
    }
    
    public function getIsDead() {
        return OperationStatusHelper::isDead($this->getStatus());
    }

    public function setIsScheduled() {
        return $this->setStatus(OperationStatusHelper::STATUS_SCHEDULED);
    }
    
    public function setIsQueued() {
        return $this->setStatus(OperationStatusHelper::STATUS_QUEUED);
    }
    
    public function setIsRunning() {
        return $this->setStatus(OperationStatusHelper::STATUS_RUNNING);
    }
    
    public function setIsFinished() {
        return $this->setStatus(OperationStatusHelper::STATUS_FINISHED);
    }
    
    public function setIsAborted() {
        return $this->setStatus(OperationStatusHelper::STATUS_ABORTED);
    }
    
    public function setIsDead() {
        return $this->setStatus(OperationStatusHelper::STATUS_DEAD);
    }
    
    
    public function getStatusName() {
        $status = $this->getStatus();
        return $status ? $status : $this->defaultStatus;
    }
    
    protected function getStatus() {
        return $this->owner->{$this->statusAttribute};
    }
    
    protected function setStatus($value) {
        $this->owner->{$this->statusAttribute} = $value;
        return $this->owner;
    }
    
    
}
