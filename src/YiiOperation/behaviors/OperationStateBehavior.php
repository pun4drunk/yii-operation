<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OperationStateBehavior
 *
 * @author vladislav
 */

namespace YiiOperation\behaviors;
use YiiOperation\interfaces\IOperationStateBehavior;

class OperationStateBehavior extends \CBehavior implements IOperationStateBehavior {
    
    public $defaultAction;
    public $helperClass;
    public $category;
    
    public function get($id = NULL, $action = NULL) {
        
        if (is_null($id)) {
            $id = $this->getId($action);
        }
        
        $operationHelperClass = $this->helperClass;
        return $operationHelperClass::loadState($id);
    }
    
    public function getId($action = NULL) {
        if (is_null($action)) {
            $action = $this->defaultAction;
        }
        
        return "$this->category.$action";
    }
    
}
