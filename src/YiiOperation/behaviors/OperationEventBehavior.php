<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of OperationEventBehavior
 *
 * @author vladislav
 */
namespace YiiOperation\behaviors;
use YiiComponents\behaviors\DependentBehavior;

abstract class OperationEventBehavior extends DependentBehavior {
    
    protected function dependencies() {
        return array(
            'state' => 'YiiOperation\interfaces\IOperationState',
        );
    }
    
    public function events() {
        return array();
    }
    
    public function getInvoker() {
        return $this->owner->owner;
    }
    
}
