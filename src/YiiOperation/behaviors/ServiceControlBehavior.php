<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of LongPollingBehavior
 *
 * @author vladislav
 */
namespace YiiOperation\behaviors;
use \CMap;

class ServiceControlBehavior extends OperationControlBehavior {

    public function events(){
        return CMap::mergeArray(parent::events(), array(
            'onBeforeWait'=>'checkIfAborted',
            'onAfterWait'=>'checkIfAborted',
        ));
    }
    
    protected function resume() {
        $now = time();
        if (!$this->state->isRunning 
                && $this->state->nextIteration 
                    && $this->state->nextIteration > $now ) {
            $this->owner->resume = true;
            $this->invoker->wait($this->state->nextIteration - $now);
        }
        return;
    }
    
}
