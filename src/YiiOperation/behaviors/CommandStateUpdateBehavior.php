<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of LongPollingBehavior
 *
 * @author vladislav
 */
namespace YiiOperation\behaviors;


class CommandStateUpdateBehavior extends ServiceStateUpdateBehavior {

    public function beforeOperation() {
       
        if (!parent::beforeOperation()) {
            return false;
        }
        
        $this->state->setProcessId();
        return true;
    }
    
    public function afterOperation() {
        parent::afterOperation();
        
        $this->state->deleteProcessId();
    }
    
}
