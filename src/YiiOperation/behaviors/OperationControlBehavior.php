<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of LongPollingBehavior
 *
 * @author vladislav
 * @owner instanceof OperationBehavior
*/
namespace YiiOperation\behaviors;

class OperationControlBehavior extends OperationEventBehavior {

    public function events(){
        return array(
            'onBeforeIteration' => 'beforeIteration',
            'onBeforeOperation' => 'beforeOperation',
        );
    }
    
    public function beforeIteration() {
        $this->checkIfAborted();
        $this->resume();
        return true;
    }
    
    protected function checkIfAborted() {
        if ($this->state->isAborted) {
            $this->state->abort();
            return false;
        }
        return true;
    }
    
    public function beforeOperation() {
        return true;
    }
    
    protected function resume() {
        return true;
    }
    
    
}
