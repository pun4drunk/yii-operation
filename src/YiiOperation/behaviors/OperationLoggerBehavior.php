<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of OperationLoggerBehavior
 *
 * @author vladislav
 */
namespace YiiOperation\behaviors;
use YiiComponents\behaviors\LoggerBehavior;

class OperationLoggerBehavior extends LoggerBehavior {
    
    public $requireCategory = false;
    
    public function events(){
        return array(
            'onBeforeOperation'=>'beforeOperation',
            'onAfterOperation'=>'afterOperation',
        );
    }
    
    public function getInvoker() {
        return $this->owner->owner;
    }
    
    public function beforeOperation() {
        $this->addInfo('operation start '.json_encode($this->invoker->args));
    }
    
    public function afterOperation() {
        $this->addInfo("operation end: code[{$this->invoker->result}]");
    }
    
}
