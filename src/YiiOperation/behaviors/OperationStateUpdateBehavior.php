<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of LongPollingBehavior
 *
 * @author vladislav
 * @owner instanceof OperationBehavior
 */
namespace YiiOperation\behaviors;
use YiiOperation\exceptions\OperationWontStartException;

class OperationStateUpdateBehavior extends OperationEventBehavior {

    public function events(){
        return array(
            'onBeforeOperation'=>'beforeOperation',
            'onAfterOperation'=>'afterOperation',
            'onDead' => 'dead',
        );
    }
    
    public function dead() {
        $this->state->setIsDead();
        return false;
    }
    
    public function beforeOperation() {
        
        if ($this->state->isRunning 
                && !$this->owner->resume) 
        {
            throw new OperationWontStartException("Operation is already running. You can resume or restart passing option into ".get_class($this->owner));
        }
        
        $this->state->setIsRunning();
    
        return true;
    }
 
    public function beforeIteration(){
        return true;
    }
    
    public function afterIteration() {
        $this->state->setLastIteration(time());
    }
    
    public function afterOperation() {
        
        if (!$this->state->isDead) {
            $this->state->setIsFinished();
        }
        
    }
    
    
}
