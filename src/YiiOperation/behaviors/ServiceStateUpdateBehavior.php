<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of LongPollingBehavior
 *
 * @author vladislav
 * @owner instanceof OperationBehavior
 */
namespace YiiOperation\behaviors;
use \CMap;

class ServiceStateUpdateBehavior extends OperationStateUpdateBehavior {

    public function events(){
        return CMap::mergeArray(parent::events(), array(
            'onBeforeIteration'=>'beforeIteration',
            'onAfterIteration'=>'afterIteration',
            'onBeforeWait'=>'beforeWait',
            'onAfterWait'=>'afterWait',
        ));
    }
    
    public function beforeIteration(){
        $this->state->deleteNextIteration();
        return true;
    }
    
    public function afterIteration() {
        
        if ($this->invoker->lastIterationResult) {
            $this->state->addSuccess();
        } else {
            $this->state->addFailure();
        }
        
        $this->state->setLastResult($this->invoker->lastIterationResult ? 1 : 0);
        $this->state->setLastIteration(time());
    }
    
    public function beforeOperation() {
        
        if (!parent::beforeOperation()) {
            return false;
        }
        $this->state->setLastRun(time());
        return true;
    }
    
    public function afterOperation() {
        $this->state->setLastResult($this->invoker->result);
        $this->state->deleteLastIteration();    
        return parent::afterOperation();
    }
    
    public function beforeWait() {
        if ($value = $this->invoker->nextIteration) {
            $this->state->setNextIteration($value);
        }
        $this->state->setIsWaiting();
        return true;
    }
    
    public function afterWait() {
        $this->state->setIsWorking();
    }
    
    
}
