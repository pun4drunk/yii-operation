<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vladislav
 */
namespace YiiOperation\interfaces;

interface IServiceState extends IOperationState {
    
    const STATUS_WAITING = 'waiting';
    const STATUS_WORKING = 'working';
    const RESULT_NONE = 'none';
}
