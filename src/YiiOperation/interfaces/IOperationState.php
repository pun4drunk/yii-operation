<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author vladislav
 */
namespace YiiOperation\interfaces;

interface IOperationState {
    
    public function reset($force = false);
    public function abort($exit = true); 

}
