<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 *
 * @author vladislav
 */
namespace YiiOperation\interfaces;

interface IOperations {
    
    public function getOperation($id, $method, $args = array());
    public function runOperation($method, $args = array(), $id = NULL);
    public function handleException(Exception $e, $isChild = false);
    
}
