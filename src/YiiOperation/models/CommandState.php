<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of FOperationStateModel
 *
 * @author vladislav
 */
namespace YiiOperation\models;
use \CMap;
use \CException;

class CommandState extends ServiceState {
    
    public function setProcessId($value = NULL) {
        if (is_null($value)) {
            $value = getmypid();
        }
        $this->processId = $value;
    }
    
    public function attributes() {
        return CMap::mergeArray(parent::attributes(), array(
            'processId' => false,
        ));
    }
    
    public function getInfoAttributes() {
        return CMap::mergeArray(parent::getInfoAttributes(), array(
            'processId',
        ));
    }
    
    public function reset($force = false) {
        
        if ($this->processId && false === $force) {
            throw new CException("pid exists: $this->processId");
        }
        
        return parent::reset($force);
    }
    
    public function getIsRunning() {
        return $this->processId || parent::getIsRunning();
    }
    
    public function abort($exit = true) {
        
        if (!$this->isWorking && $this->processId) {
            exec("kill $this->processId");
            $this->deleteProcessId();
        }
        
        parent::abort($exit);
    }
    
}
