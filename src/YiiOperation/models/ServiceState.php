<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of FOperationStateModel
 *
 * @author vladislav
 */
namespace YiiOperation\models;
use \CMap;

class ServiceState extends OperationState implements YiiOperation\interfaces\IServiceState {
    
    public $dateFormat = 'Y-m-d H:i:s';
    public $protectedAttributes = array('nextIteration');
    
    public function getInfoAttributes() {
        return array(
            'id',
            'lastResult','lastResultCount',
            'lastError',
            'statusName',
            'countSuccess','countFailure',
            'meta',
        );
    }
    
    public function attributes() {
        return CMap::mergeArray(parent::attributes(), array(
            'nextIteration' => false,
            'countSuccess' => 0,
            'countFailure' => 0,
            'lastRun' => false,
            'lastResult' => false,
            'lastError' => "",
            'lastResultCount' => 0,
            'meta' => array(),
        ));
    }
    
    public function setMeta($name, $value) {
        $meta = $this->meta;
        if (false === $meta) {
            $meta = array();
        }
        
        $meta[$name] = $value;
        $this->meta = $meta;
    }
    
    public function getIsWaiting() {
        return self::STATUS_WAITING === $this->status;
    }
    
    public function getIsWorking() {
        return self::STATUS_WORKING === $this->status;
    }
    
    public function setIsWaiting() {
        $this->status = self::STATUS_WAITING;
    }
    
    public function setIsWorking() {
        $this->status = self::STATUS_WORKING;
    }
    
    public function setLastResult($value) {
        
        $lastResult = $this->lastResult;
        $this->lastResult = $value;
        
        if ($value === $lastResult) {
            $this->lastResultCount++;
        } else {
            $this->lastResultCount = 1;
        }
    }    
    
    public function getIsRunning() {
        return in_array($this->status, array(
            self::STATUS_WORKING,
            self::STATUS_WAITING,
            self::STATUS_RUNNING,
        ));
    }
    
    public function addSuccess() {
        $this->countSuccess++;
    }
    
    public function addFailure() {
        $this->countFailure++;
    }
    
    public function ping($time = NULL){
        $time = is_null($time) ? time() : $time;
        if ($this->isRunning 
                && $this->nextIteration 
                    && $this->nextIteration < $time) {
            $this->setIsDead();
        }
        return $this;
    }
    
    public function getNextIterationDate() {
        $nextIteration = $this->nextIteration;
        return $nextIteration ? date($this->dateFormat, $nextIteration) : NULL;
    }
    
}
