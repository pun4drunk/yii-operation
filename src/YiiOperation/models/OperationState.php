<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of FOperationStateModel
 *
 * @author vladislav
 */
namespace YiiOperation\models;
use \Yii;
use \CMap;

Yii::import('components.models.CacheModel');


class OperationState extends \CacheModel implements YiiOperation\interfaces\IOperationState {
    
    public $protectedAttributes = array();
    
    public function attributeNames() {
        return array_keys($this->_attributes);
    }
    
    public function attributes() {
        return array(
            'status' => $this->defaultStatus,
            'lastIteration' => false,
        );
    }
    
    public function getInfoAttributes() {
        return array_merge(array_keys($this->_attributes), array(
            'id',
        ));
    }

    public function behaviors() {
        return CMap::mergeArray(parent::behaviors(), array(
            'attributesBehavior' => array(
                'class' => 'OperationStateAttributesBehavior',
            )
        ));
    }
    
    public function reset($force = false){
        $protected = $force ? array() : $this->protectedAttributes;
        return $this->unsetAttributes(
            array_diff(array_keys($this->_attributes), $this->protectedAttributes)
        );
    }
    
    public function getInfo($attributes = NULL) {
        
        $attributes = is_null($attributes) ? $this->getInfoAttributes() : $attributes;
        
        $info = array();
        foreach ($attributes as $key) {
            $method = 'get'.ucfirst($key);
            $info[$key] = $this->$method();
        }
        
        return $info;
    }

    public function abort($exit = true) {
        $this->setIsAborted();
        if ($exit) {
            exit;
        }
    }
    
}
