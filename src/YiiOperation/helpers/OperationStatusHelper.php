<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of OperationStatusHelper
 *
 * @author vladislav
 */
namespace YiiOperation\helpers;

abstract class OperationStatusHelper {
    
    const STATUS_NOT_RUNNING    = 'not running';
    const STATUS_RUNNING        = 'running';
    const STATUS_SCHEDULED      = 'scheduled';
    const STATUS_QUEUED         = 'queued';
    const STATUS_FINISHED       = 'finished';
    const STATUS_ABORTED        = 'aborted';
    const STATUS_DEAD           = 'dead';
    
    public static function isRunning($status) 
    {
        return self::STATUS_RUNNING === $status;
    }
    
    public static function isFinished($status) 
    {
        return self::STATUS_FINISHED === $status;
    }
    
    public static function isAborted($status) 
    {
        return self::STATUS_ABORTED === $status;
    }
    
    public static function isDead($status) 
    {
        return self::STATUS_DEAD === $status;
    }
    
    public static function isScheduled($status) 
    {
        return self::STATUS_SCHEDULED === $status;
    }
    
    public static function isQueued($status) 
    {
        return self::STATUS_QUEUED === $status;
    }
    
    
}
