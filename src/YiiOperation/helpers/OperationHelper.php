<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of FOperationHelper
 *
 * @author Vladislav
 */
namespace YiiOperation\helpers;
use \CException;

abstract class OperationHelper {
    
    public static $stateClass = 'OperationState';
    const GLOBAL_PREFIX = 'operation';
    const ENTITY = 'operation';

    protected static $_operations = array();

    public static function abort($alias, $exit = false) {
        
        $state = self::loadState($alias);
        
        if (!$state->isRunning) {
            throw new CException(static::ENTITY." $alias is not running");
        }
        
        $state->abort($exit);
        return true;
    }
    
    public static function reset($alias, $force = false) {
        
        $state = self::loadState($alias);
        $state->reset($force);
        
        return true;
    }
    
    public static function loadState($alias) {
        if (!isset(static::$_operations[$alias])) {
            $stateClass = static::$stateClass;
            $_alias = strlen(static::GLOBAL_PREFIX) ? static::GLOBAL_PREFIX.'.'.$alias : $alias;
            static::$_operations[$alias] = new $stateClass($_alias);
        }
        return static::$_operations[$alias];
    }
    
}
