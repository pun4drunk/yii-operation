<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */

/**
 * Description of LongPollingBehavior
 *
 * @author vladislav
 */
namespace YiiOperation\components;
use CComponent;

class Operation extends CComponent {
    
    public $alias;
    public $object;
    public $method;
    public $args = array();
    
    public $result;
    
    public $controlBehavior = array(
        'class' => 'YiiOperation\behaviors\OperationControlBehavior'
    );
    
    public $updaterBehavior = array(
        'class' => 'YiiOperation\behaviors\OperationStateUpdateBehavior',
    );
    
    public $loggerBehavior = array(
        'class' => 'YiiComponents\behaviors\LoggerBehavior',
        'category' => "application.operations",
    );
    
    public $operationBehaviorClass = 'YiiOperation\behaviors\OperationBehavior';
    public $operationHelperClass = 'YiiOperation\helpers\OperationHelper';
    public $operationLoggerClass = 'YiiOperation\behaviors\OperationLoggerBehavior';
    
    public function getStateObject() {
        $operationHelperClass = $this->operationHelperClass;
        return $operationHelperClass::loadState($this->alias);
    }
    
    public function init() {
        $this->attachOperationBehavior();
        $this->attachLoggerBehavior();
    }
    
    public function attachOperationBehavior() {
        $this->attachBehavior('operation', array(
            'class' => $this->operationBehaviorClass,
            'behaviors' => $this->operationBehaviors(),
            'state' => $this->getStateObject(),
        ));
    }
    
    public function attachLoggerBehavior() {
        $this->attachBehavior('logger', $this->loggerBehavior);
    }
    
    public function operationBehaviors() {
        return array(
            'control'   => $this->controlBehavior,
            'updater'   => $this->updaterBehavior,
            'logger'    => $this->operationLoggerBehavior,
        );
    }
    
    protected function getOperationLoggerBehavior() {
        return \CMap::mergeArray($this->loggerBehavior, array(
            'class' => $this->operationLoggerClass,
        ));
    }
    
    public function prepare() {
        $this->init();
        return $this;
    }
    
    public function execute() {
        
        if (!$this->beforeOperation()) {
            return false;
        }
        
        try{
            $this->result = $this->_execute();
        } catch (\Exception $e) {
            $this->result = $this->handleException($e);
        }
        $this->afterOperation();
        
        return $this->result;
        
    }
    
    protected function _execute() {
        return call_user_func_array(array($this->object, $this->method), $this->args);
    }
    
    protected function handleException(\Exception $e) {
        $this->stateObject->setLastError($e->getMessage());
        $this->operation->dead();
        throw $e;
    }
    
    public function abort($exit = true) {
        $this->stateObject->abort($exit);
    }
    
    public function reset($force = false) {
        $this->stateObject->reset($force);
    }
}
