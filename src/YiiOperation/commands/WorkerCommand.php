<?php
namespace YiiOperation\commands;
use \CMap;

abstract class WorkerCommand extends ServiceCommand {
    
    public $daemonMode = false;
    public $maxCnt = NULL;
    public $freq = NULL;
    public $freqRetry = NULL;
    public $randomFactor = NULL;
    public $iteration = 0;

    public $serviceName = 'console.worker';

    public $daemonOperationClass = 'DaemonOperation';
    
    public function handleException(Exception $e, $isChild = false) {
        
        if ($isChild) {
            throw $e;
        }
        
        return parent::handleException($e, $isChild);
    }
    
    public function getDaemonParams() {
        
        $params = array();
        foreach (array(
            'maxCnt' => NULL,
            'freq' => NULL,
            'randomFactor' => NULL,
            'freqRetry' => NULL,
        ) as $name => $value) {
            $params[$name] = $this->$name;
        }
        
        return $params;
    }
    
    public function getOperationClass($method = NULL) {
        
        $operationClass = $this->isDemonized($method) 
                        ? $this->daemonOperationClass
                        : $this->operationClass;
        
        return $operationClass;
    }
    
    public function getOperation($id, $method, $args = array()) {
        
        $this->logger->addTrace("initializing $method daemon...");
        return parent::getOperation($id, $method, $args);
    }
    
    public function getOperationParams($id, $method, $args) {
        
        $params = parent::getOperationParams($id, $method, $args);
        
        if ($this->isDemonized($method)) {
            $params = CMap::mergeArray($params, array(
                'daemonParams' => $this->daemonParams,
            ));
        }
        
        return $params;
    }
    
    protected function isDemonized($method) {
        return $this->daemonMode && $method === $this->action;
    }
    
    public function getOperationStateInfo() {
        
        if ($this->daemonMode) {
            return array(
                'lastIterationAgo',
                'nextIterationIn',
                'statusName',
                'processId',
            );
        }
        
        return parent::getOperationStateInfo();
    }
   
}