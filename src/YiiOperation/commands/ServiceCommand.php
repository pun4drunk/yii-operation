<?php

/*
 * @category  Projects
 * @package   self.socialFella
 * @author    Vladislav Waxme <vlad.waxme@gmail.com>
 * @copyright 2014 Vladislav Waxme
 * @license   http://www.opensource.org/licenses/bsd-license.php New BSD Licence
 * @version   
 * @link      
 */
    
/**
 * Description of FConsoleCommand
 *
 * @author vladislav
 */
namespace YiiOperation\commands;
use YiiComponents\helpers\AppHelper;
use YiiComponents\commands\LoggedConsoleCommand;

abstract class ServiceCommand extends LoggedConsoleCommand {
    
    public $getState = false;
    public $reset = false;
    public $force = false;
    public $abort = false;
    protected $_action;
    
    public $appName = 'application';
    
    public $operationClass = 'Operation';
    
    public function handleException(Exception $e, $isChild = false) {
        
        if ($this->debug) {
            throw $e;
        } else {
            $this->logger->addError($e->getMessage(), $this->action);
        }
        return false;
    }
    
    public function beforeAction($action, $params) {
        
        if (!parent::beforeAction($action, $params)) {
            return false;
        }
        
        $this->action = $action;
        
        $this->attachBehaviors(array(
            'cache' => $this->cacheBehavior(),
            'operationRunner' => $this->operationRunnerBehavior($action),
        ));
        
        return true;
    }
    
    protected function operationStateBehavior() {
        return array(
            'class'             => 'operation.behaviors.OperationStateBehavior',
            'defaultAction'     => $this->action,
            'category'          => $this->categoryPrefix,
            'helperClass'       => 'operation.helpers.OperationHelper',
        );
    }
    
    protected function printOperationState($alias) {
        
        $this->attachBehavior('operationState', $this->operationStateBehavior());

        $this->logger->addInfo("Retrieving $alias state...");
        $this->logger->addInfo(json_encode($this->operationState->get($alias)->info, JSON_PRETTY_PRINT));
        
        return;
    }
    
    public function getOperationClass($method = NULL) {
        return $this->operationClass;
    }
    
    public function getAction() {
        return $this->_action;
    }
    
    protected function setAction($action) {
        $this->_action = $action;
    }
    
    protected function cacheBehavior() {
        return array(
            'class' => 'YiiComponents\behaviors\CacheBehavior',
            'cacheKeyPrefix' => $this->getUniqueId(),
        );
    }
    
    protected function operationRunnerBehavior() {
        return array(
            'class' => 'YiiOperation\behaviors\OperationRunnerBehavior',
            'uniqueId' => $this->getUniqueId(),
            'operationClass' => $this->getOperationClass($action),
            'abort' => $this->abort,
            'reset' => $this->reset,
            'force' => $this->force,
        );
    }
    
    
    abstract protected function getUniqueId();

    public function runOperation($method, $args = array()) {
        
        $alias = $this->operationRunner->getAlias($method);
        if ($this->getState) {
            return $this->printOperationState($alias);
        } else $this->operationRunner->run($method, $args);
        
        return 1;
    }
    
}


